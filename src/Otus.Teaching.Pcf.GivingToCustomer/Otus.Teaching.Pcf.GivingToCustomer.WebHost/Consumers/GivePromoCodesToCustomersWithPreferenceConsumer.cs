﻿using IHateYouMassTransitCommonNamespace;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivePromoCodesToCustomersWithPreferenceConsumer : IConsumer<IPromoCodeDTO>
    {
        private readonly IPromoCodeService _promoCodesService;

        public GivePromoCodesToCustomersWithPreferenceConsumer(
            IPromoCodeService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeDTO> context)
        {
            await _promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
