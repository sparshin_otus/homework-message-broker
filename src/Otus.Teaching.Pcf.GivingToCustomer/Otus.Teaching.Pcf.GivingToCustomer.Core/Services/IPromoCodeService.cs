﻿using IHateYouMassTransitCommonNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IPromoCodeService
    {
        Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(IPromoCodeDTO message);
    }
}
