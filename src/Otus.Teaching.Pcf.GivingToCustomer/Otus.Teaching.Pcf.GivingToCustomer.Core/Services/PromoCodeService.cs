﻿using IHateYouMassTransitCommonNamespace;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromoCodeService(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        public async Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(IPromoCodeDTO message)
        {
            var preference = await _preferencesRepository.GetByIdAsync(message.PreferenceId);

            if (preference is null)
            {
                throw new ArgumentNullException($"Preference with ID: {message.PreferenceId} not found");
            }

            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(message, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return true;
        }
    }
}
