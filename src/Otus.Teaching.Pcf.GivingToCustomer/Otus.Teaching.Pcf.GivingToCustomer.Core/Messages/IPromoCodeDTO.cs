﻿using System;

namespace IHateYouMassTransitCommonNamespace
{
    public interface IPromoCodeDTO
    {
        public Guid PromoCodeId { get; set; }
        public string PromoCode { get; set; }
        public string ServiceInfo { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public Guid PartnerId { get; set; }
        public Guid? PartnerManagerId { get; set; }
        public Guid PreferenceId { get; set; }

    }
}
