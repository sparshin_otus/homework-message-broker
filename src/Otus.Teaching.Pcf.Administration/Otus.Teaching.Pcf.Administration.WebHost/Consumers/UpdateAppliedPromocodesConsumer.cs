﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Services;
using System.Threading.Tasks;
using System;
using IHateYouMassTransitCommonNamespace;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class UpdateAppliedPromocodesConsumer : IConsumer<IPromoCodeDTO>
    {
        private readonly IEmployeeService _employeeService;

        public UpdateAppliedPromocodesConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeDTO> context)
        {
            if (!context.Message.PartnerManagerId.HasValue)
            {
                throw new ArgumentNullException($"Partner Manager ID is null");
            }

            await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }

}
