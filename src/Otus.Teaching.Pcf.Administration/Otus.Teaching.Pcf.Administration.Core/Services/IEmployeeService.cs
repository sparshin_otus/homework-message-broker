﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public interface IEmployeeService
    {
        Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }

}
